package com.example.focus.model

data class Todo(val text: String, val id: Long, val status: Boolean = false) {

}