package com.example.focus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.focus.util.NotificationUtil
import com.example.focus.util.PrefUtil

class TimerNotificationActionReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action){
                AppConstants.ACTION_STOP -> {
                    MainTimerActivity.removeAlarm(context)
                    PrefUtil.setTimerState(MainTimerActivity.TimerState.Stopped, context)
                    NotificationUtil.hideTimerNotification(context)
                }
                AppConstants.ACTION_PAUSE -> {
                    var secondsRemaining = PrefUtil.getSecondsRemaining(context)
                    val alarmSetTime = PrefUtil.getAlarmSetTime(context)
                    val nowSeconds = MainTimerActivity.nowSeconds

                    secondsRemaining -= nowSeconds - alarmSetTime
                    PrefUtil.setSecondsRemaining(secondsRemaining, context)

                    MainTimerActivity.removeAlarm(context)
                    PrefUtil.setTimerState(MainTimerActivity.TimerState.Paused, context)
                    NotificationUtil.showTimerPaused(context)
                }
                AppConstants.ACTION_RESUME -> {
                    val secondsRemaining = PrefUtil.getSecondsRemaining(context)
                    val wakeUpTime = MainTimerActivity.setAlarm(context, MainTimerActivity.nowSeconds, secondsRemaining)
                    PrefUtil.setTimerState(MainTimerActivity.TimerState.Running, context)
                    NotificationUtil.showTimerRunning(context, wakeUpTime)
                }
                AppConstants.ACTION_START -> {
                    val minutesRemaining = PrefUtil.getTimerLength(context)
                    val secondsRemaining = minutesRemaining * 60L
                    val wakeUpTime = MainTimerActivity.setAlarm(context, MainTimerActivity.nowSeconds, secondsRemaining)
                    PrefUtil.setTimerState(MainTimerActivity.TimerState.Running, context)
                    PrefUtil.setSecondsRemaining(secondsRemaining, context)
                    NotificationUtil.showTimerRunning(context, wakeUpTime)
                }
            }
        }
    }
