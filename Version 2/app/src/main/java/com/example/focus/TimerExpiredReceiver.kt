package com.example.focus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.focus.util.NotificationUtil
import com.example.focus.util.PrefUtil

class TimerExpiredReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        NotificationUtil.showTimerExpired(context)

        PrefUtil.setTimerState(MainTimerActivity.TimerState.Stopped, context)
        PrefUtil.setAlarmSetTime(0, context)
    }
}
