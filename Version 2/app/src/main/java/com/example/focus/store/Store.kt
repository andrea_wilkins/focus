package com.example.focus.store

import android.arch.core.util.Function
import com.example.focus.model.Action

interface Store<T> {
    fun dispatch(action: Action)

    fun subscribe( renderer: Renderer<T>, func: Function<T, T> = Function { it })
}