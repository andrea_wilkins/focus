package com.example.focus.model


data class TodoModel(val todos: List<Todo>, val visibility: Visibility)