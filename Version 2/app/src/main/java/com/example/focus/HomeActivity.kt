package com.example.focus

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.home_page.*


class HomeActivity: AppCompatActivity() {

//    @SuppressLint("MissingSuperCall")
    private fun render(savedInstanceState: Bundle?) {

        create.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent);
            Log.d("myTag", "This is my message");
            setContentView(R.layout.activity_main)
        }
    }
}