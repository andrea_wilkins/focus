Focus

What is Focus?
Focus is a productivity application that allows users to be more productive over short periods of time, by prompting them to create a to do list and setting a timer for each task at hand.
The app is designed to function in such a way that you only really use it in one sitting. You can sit down, decide what you want to do and then create to do’s for each individual step required to accomplish your main goal. 

How to run the application:

Clone the repository onto your local device.
Open the project in Android Studio.
Click on play to run the app.
Select the Nexus 5 as your device.


Features and functionality:

A main page where the user can add to dos to their list
A button that filters the list items by complete and incomplete items
A timer that counts down even when the application is exited
The ability to pause, play and stop the timer
The ability to change the duration of the timer
The ability to control the timer in the notification bar